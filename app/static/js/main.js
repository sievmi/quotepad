$(document).ready(function() {
  function visibleContent() {
    if (!content.length) return;

    for (var i = 0; i < content.length; i++) {
      content[i].style.display = 'none';
    }

    content[count].style.display = 'block';
  }

  function increaseCount() {
    count++;
    if (count == content.length)
      count = 0;

    visibleContent();
  }

  function decreaseCount() {
    if (count) {
      count--;
    }
    else {
      count = content.length - 1;
    }

    visibleContent();
  }

  var content = document.querySelectorAll('div[id |= "content"]');
  var count = 0;

  visibleContent();

  $("#right-scroll").click(function() {
    increaseCount();
  });

  $("#left-scroll").click(function() {
    decreaseCount();
  });

  $(document).keydown(function(e) {
    var event = new Event('click', {bubbles: true, cancelable: true});

    if (e.keyCode == 37) {
      var leftScroll = document.getElementById('left-scroll');
      leftScroll.dispatchEvent(event);
      return false;
    }

    if (e.keyCode == 39) {
      var rightScroll = document.getElementById('right-scroll');
      rightScroll.dispatchEvent(event);
      return false;
    }
  });

  $(".favorites").click(function() {
    var x = $(this).siblings(".quotes-id");
    $(this).siblings(".favorites").toggle();
    var cntFans = $(this).siblings(".count-favorites")[0].innerHTML;

    if(this.innerHTML=="Удалить из избранного") {
      cntFans = Number(cntFans) - 1;
    } else {
      cntFans = Number(cntFans) + 1;
    }

    $(this).siblings(".count-favorites")[0].innerHTML = cntFans;

    $(this).toggle();
    var ID = $(x).text().trim();

    $.post("/toggleFavorites",
      {
        ID: ID,
      },
      function(data, status) {
      });
  });

  $(".removeQuote").click(function() {
    var x = $(this).parent().siblings(".quotes-id");
    var ID = $(x).text().trim();
    $.post("/removeQuote",
      {
        ID: ID
      },
      function(data, status) {
      });

    $(this).parent().parent().hide('slow');

    var accountQuotesLength = document.getElementById('accountQuotesLength');
    var quotesLength = accountQuotesLength.innerHTML.match(/\b\d+\b/);
    accountQuotesLength.innerHTML = "Number of quotes: " + (quotesLength - 1);
  });

  $("#changePassword").click(function(){
    $("#changePasswordForm").show();
    $("#AcceptedChangePassword").hide();
    $(this).hide();
  });

  $("#changePassBtn").click(function(){
    var newPassword = $("#newPassword").val().trim();

    $.post("/changePassword",
        {
            newPassword: newPassword
        }
    ), function(data, status) {
        alert(data);
    }

    $("#changePasswordForm").hide();
    $("#AcceptedChangePassword").show();
    $("#changePassword").show();
  });

  $('.remove-message').click(function() {
    $(this).parent().hide();
  });
});
