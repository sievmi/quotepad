from app import db

favorites = db.Table('favorites',
                     db.Column('user_id', db.Integer, db.ForeignKey('user.id')),
                     db.Column('quote_id', db.Integer, db.ForeignKey('quote.id')),
                     db.UniqueConstraint('user_id', 'quote_id', name='UC_user_id_quote_id'),
)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), unique=True)
    password = db.Column(db.String(64))

    business = db.Column(db.Boolean)
    sport = db.Column(db.Boolean)
    guys = db.Column(db.Boolean)
    girls = db.Column(db.Boolean)
    philosophy = db.Column(db.Boolean)
    books = db.Column(db.Boolean)
    films = db.Column(db.Boolean)
    songs = db.Column(db.Boolean)
    motivation = db.Column(db.Boolean)
    art = db.Column(db.Boolean)
    poems = db.Column(db.Boolean)
    love = db.Column(db.Boolean)
    humor = db.Column(db.Boolean)
    science = db.Column(db.Boolean)

    quotes = db.relationship('Quote', backref = 'author', lazy = 'dynamic')

    favQuotes = db.relationship('Quote', secondary=favorites,
                                backref = db.backref('fan', lazy = 'dynamic'), lazy = 'dynamic')

    def favoriteQuotes(self):
        return Quote.query.filter(Quote.fan.any(id=self.id)).all()

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def __repr__(self):
        return '<User %r>' % (self.username)

class Quote(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String(1000))

    business = db.Column(db.Boolean)
    sport = db.Column(db.Boolean)
    guys = db.Column(db.Boolean)
    girls = db.Column(db.Boolean)
    philosophy = db.Column(db.Boolean)
    books = db.Column(db.Boolean)
    films = db.Column(db.Boolean)
    songs = db.Column(db.Boolean)
    motivation = db.Column(db.Boolean)
    art = db.Column(db.Boolean)
    poems = db.Column(db.Boolean)
    love = db.Column(db.Boolean)
    humor = db.Column(db.Boolean)
    science = db.Column(db.Boolean)

    owner = db.Column(db.String(100))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    cntFans = db.Column(db.Integer)

    def is_liked(self, user):
        return self.fan.filter(favorites.c.user_id == user.id).count() > 0

    def like(self, user):
        if not self.is_liked(user):
            self.fan.append(user)
            self.cntFans = self.cntFans + 1
            return self

    def unlike(self, user):
        if self.is_liked(user):
            self.fan.remove(user)
            self.cntFans = self.cntFans - 1
            return self




