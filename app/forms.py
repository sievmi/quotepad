from flask.ext.wtf import Form
from wtforms import TextAreaField, StringField, PasswordField, BooleanField, validators
from wtforms.validators import Required

class LoginForm(Form):
    loginUsername = StringField('Login Username', id = "signInUser", validators = [Required()])
    loginPassword = PasswordField('Login Password', id = "signInPassword", validators = [Required()])
    remember_me = BooleanField("Remember me", id = "signInRememberMe")

class RegisterForm(Form):
    registerUsername = StringField("Register Username", id = "signUpUser", validators = [Required()])
    registerPassword = PasswordField("Register Password", id = "signUpPassword", validators = [
        validators.Required(),
        validators.EqualTo('confirm', message='Password must be match')])
    confirm = PasswordField('Repeat password')
    business = BooleanField("Business", id = "business")
    sport = BooleanField("Sport", id = "sport")
    guys = BooleanField("Guys", id = "guys")
    girls = BooleanField("Girls", id = "girls")
    philosophy = BooleanField("Philosophy", id = "philosophy")
    books = BooleanField("Books", id = "books")
    films = BooleanField("Films", id = "films")
    songs = BooleanField("Songs", id = "songs")
    motivation = BooleanField("Motivation", id = "motivation")
    art = BooleanField("Art", id = "art")
    poems = BooleanField("Poems", id = "poems")
    love = BooleanField("Love", id = "love")
    humor = BooleanField("Humor", id = "humor")
    science = BooleanField("Science", id = "science")

class FindForm(Form):
    findBusiness = BooleanField("Business", id = "findBusiness")
    findSport = BooleanField("Sport", id = "findSport")
    findGuys = BooleanField("Guys", id = "findGuys")
    findGirls = BooleanField("Girls", id = "findGirls")
    findPhilosophy = BooleanField("Philosophy", id = "findPhilosophy")
    findBooks = BooleanField("Books", id = "findBooks")
    findFilms = BooleanField("Films", id = "findFilms")
    findSongs = BooleanField("Songs", id = "findSongs")
    findMotivation = BooleanField("Motivation", id = "findMotivation")
    findArt = BooleanField("Art", id = "findArt")
    findPoems = BooleanField("Poems", id = "findPoems")
    findLove = BooleanField("Love", id = "findLove")
    findHumor = BooleanField("Humor", id = "findHumor")
    findScience = BooleanField("Science", id = "findScience")

class FindAuthorForm(Form):
    findAuthor = StringField("Author", validators = [Required()])


class AddQuoteForm(Form):
    text = TextAreaField("Text", validators = [Required()])
    owner = StringField("Owner", validators = [Required()])
    business = BooleanField("Business", id = "business")
    sport = BooleanField("Sport", id = "sport")
    guys = BooleanField("Guys", id = "guys")
    girls = BooleanField("Girls", id = "girls")
    philosophy = BooleanField("Philosophy", id = "philosophy")
    books = BooleanField("Books", id = "books")
    films = BooleanField("Films", id = "films")
    songs = BooleanField("Songs", id = "songs")
    motivation = BooleanField("Motivation", id = "motivation")
    art = BooleanField("Art", id = "art")
    poems = BooleanField("Poems", id = "poems")
    love = BooleanField("Love", id = "love")
    humor = BooleanField("Humor", id = "humor")
    science = BooleanField("Science", id = "science")