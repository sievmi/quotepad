from forms import LoginForm, RegisterForm, AddQuoteForm, FindForm, FindAuthorForm
from flask import render_template, redirect, url_for, g, session, request, flash
from flask.ext.login import login_user, logout_user, current_user, login_required
from app import app
from app import db, lm
from models import User, Quote
from random import shuffle

@lm.user_loader
def load_user(id):
    return User.query.get(int(id))

@app.before_request
def before_request():
    g.user = current_user

@lm.unauthorized_handler
def unauthorized_callback():
    return redirect('/index')

@app.route('/<username>/liked', methods=['GET', 'POST'])
def liked(username):
    if not g.user.is_authenticated:
        return redirect(url_for('signUp'))
    loginForm = LoginForm()
    registerForm = RegisterForm()

    user = User.query.filter_by(username=username).first()
    quotes = User.favoriteQuotes(user)
    return render_template("favorites.html",
                           loginForm=loginForm, registerForm=registerForm,
                           quotes = quotes)


@app.route('/removeQuote', methods=['GET', 'POST'])
def removeQuote():
    id = request.form['ID']
    quote = Quote.query.get(id)

    db.session.delete(quote)
    db.session.commit()

    return "OK"

@app.route('/changePassword', methods=['GET', 'POST'])
def changePassword():
    newPassword = request.form['newPassword']

    user = g.user
    user.password = newPassword

    db.session.add(user)
    db.session.commit()

    return "OK"

@app.route('/toggleFavorites', methods=['GET', 'POST'])
def toggleFavorites():
    id = request.form['ID']
    quote = Quote.query.get(id)
    if not quote.is_liked(current_user):
        db.session.add(quote.like(current_user))
        db.session.commit()
    else:
        db.session.add(quote.unlike(current_user))
        db.session.commit()
    return "OK"

@app.route('/signIn', methods=['GET', 'POST'])
def signIn():
    loginForm = LoginForm()
    registerForm = RegisterForm()

    if loginForm.validate_on_submit():
        session['remember_me'] = loginForm.remember_me.data
        user = User.query.filter_by(username = loginForm.loginUsername.data).first()
        if user is None or (user.password != loginForm.loginPassword.data):
            # ADD FLASH MESSAGE ABOUT NO MATCHING PASSWORD OR LOGINS !!!!!!!!!
            flash("Login or password don't match")
            return redirect(url_for('signIn'))
        login_user(user, remember=loginForm.remember_me.data)
        return redirect(url_for('index'))
        #redirect(request.args.get('next') or url_for('index'))

    return render_template('signIn.html', loginForm=loginForm, registerForm=registerForm)

@app.route('/signUp', methods=['GET', 'POST'])
def registration():
    loginForm = LoginForm()
    registerForm = RegisterForm()

    if registerForm.validate_on_submit():
        user = User.query.filter_by(username = registerForm.registerUsername.data).first()
        if user is not None:
            flash("This user has already registered.")
            flash("Please, choose another username")
            # ADD FLASH MESSAGE: THIS USER HAVE ALREADY REGISTERED !!!!!!!!!
            return redirect(url_for('registration'))
        newUser = User(username = registerForm.registerUsername.data, password = registerForm.registerPassword.data,
            business = registerForm.business.data, sport = registerForm.sport.data,
            guys = registerForm.guys.data, girls = registerForm.girls.data,
            philosophy = registerForm.philosophy.data, books = registerForm.books.data,
            films = registerForm.films.data, songs = registerForm.songs.data,
            motivation = registerForm.motivation.data, art = registerForm.art.data,
            poems = registerForm.poems.data, love = registerForm.love.data,
            humor = registerForm.humor.data, science = registerForm.science.data)

        db.session.add(newUser)
        db.session.commit()
        login_user(newUser, remember=False)
        return redirect(url_for('index'))
    else:
        if(registerForm.registerUsername.data):
            flash("Passwords don't math")

    return render_template("signUp.html", loginForm=loginForm, registerForm=registerForm)

@app.route('/add', methods=['GET', 'POST'])
def add():
    if not g.user.is_authenticated:
        return redirect(url_for('registration'))
    loginForm = LoginForm()
    registerForm = RegisterForm()

    addForm = AddQuoteForm()
    user = User.query.filter_by(username=g.user.username).first()
    if addForm.validate_on_submit():
        text = addForm.text.data
        owner = addForm.owner.data

        newQuote= Quote(text = addForm.text.data,
            business = addForm.business.data, sport = addForm.sport.data,
            guys = addForm.guys.data, girls = addForm.girls.data,
            philosophy = addForm.philosophy.data, books = addForm.books.data,
            films = addForm.films.data, songs = addForm.songs.data,
            motivation = addForm.motivation.data, art = addForm.art.data,
            poems = addForm.poems.data, humor = addForm.humor.data, owner = addForm.owner.data,
            science = addForm.science.data, love = addForm.love.data, author = user, cntFans=0)
        db.session.add(newQuote)
        db.session.commit()
        flash("Quote has added succesfully")
        return redirect(url_for('add'))

    return render_template('add.html',
            loginForm = loginForm, registerForm = registerForm,
            addForm = addForm)



@app.route('/<username>/account', methods=['GET', 'POST'])
def account(username):
    if not g.user.is_authenticated:
        return redirect(url_for('registration'))
    loginForm = LoginForm()
    registerForm = RegisterForm()


    user = User.query.filter_by(username=username).first()

    quotes = Quote.query.filter_by(author=user).all()
    quotes.reverse()


    return render_template('account.html',
            loginForm = loginForm, registerForm = registerForm, quotes = quotes)



@app.route('/logout', methods=['GET', 'POST'])
def logout():
    logout_user()
    return redirect(url_for('index'))

@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():
    loginForm = LoginForm()
    registerForm = RegisterForm()
    findForm = FindForm()
    findAuthorForm = FindAuthorForm()

    allQuotes = Quote.query.all()
    cntQuotes = len(allQuotes)
    shuffle(allQuotes)
    quotes = []

    if findAuthorForm.validate_on_submit():
        for q in allQuotes:
            if(q.owner == findAuthorForm.findAuthor.data):
                quotes.append(q)

        if(len(quotes) == 1):
            emptyQuote= Quote(text = "",cntFans=0)
            quotes.append(emptyQuote)

        allQuotes = quotes
        quotes = []
        flash("Search results: ")
    elif findForm.validate_on_submit():
        for q in allQuotes:
            if filterByFlag(quote=q, business=findForm.findBusiness.data, sport=findForm.findSport.data,
                    guys=findForm.findGuys.data, girls=findForm.findGirls.data, philosophy=findForm.findPhilosophy.data,
                    books=findForm.findBooks.data, films=findForm.findFilms.data, songs=findForm.findSongs.data,
                    motivation=findForm.findMotivation.data, art=findForm.findArt.data, poems=findForm.findPoems.data,
                    love=findForm.findLove.data, humor=findForm.findHumor.data, science=findForm.findScience.data) :
                quotes.append(q)

        allQuotes = quotes
        quotes = []
        flash("Search results: ")
    elif g.user.is_authenticated:
         flag = g.user.business or g.user.sport or g.user.guys or g.user.girls or g.user.philosophy or \
             g.user.books or g.user.films or g.user.songs or g.user.motivation or g.user.art or g.user.poems or \
             g.user.love or g.user.humor or g.user.science
         if flag:

            for q in allQuotes:
                if filterByFlag(quote=q, business=g.user.business, sport=g.user.sport,
                    guys=g.user.guys, girls=g.user.girls, philosophy=g.user.philosophy,
                    books=g.user.books, films=g.user.films, songs=g.user.songs,
                    motivation=g.user.motivation, art=g.user.art, poems=g.user.poems,
                    love=g.user.love, humor=g.user.humor, science=g.user.science) :
                    quotes.append(q)

            allQuotes = quotes
            quotes = []

    for i in range(0, min(len(allQuotes)//2, 10)):
        quotes.append(allQuotes[2*i])
        quotes.append(allQuotes[2*i+1])

    if len(quotes)==0:
        flash("no results.")
    return render_template("index.html",
                           loginForm = loginForm,
                           registerForm = registerForm,
                           findForm = findForm,
                           findAuthorForm = findAuthorForm,
                           quotes = quotes,
                           cntQuotes = cntQuotes)


def filterByFlag(quote, business=False, sport=False, guys=False, girls=False, philosophy=False, books=False,
                 films=False, songs=False, motivation=False,
                 art=False, poems=False, love=False, humor=False, science=False):
    return (quote.business and business) or (quote.sport and sport) or (quote.guys and guys) or \
           (quote.girls and girls) or (quote.philosophy and philosophy) or (quote.books and books) or \
           (quote.films and films) or (quote.songs and songs) or (quote.motivation and motivation) or \
           (quote.art and art) or (quote.poems and poems) or (quote.love and love) or (quote.humor and humor) or \
           (quote.science and science)

