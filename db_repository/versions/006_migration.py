from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
quote = Table('quote', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('text', String(length=1000)),
    Column('business', Boolean),
    Column('sport', Boolean),
    Column('guys', Boolean),
    Column('girls', Boolean),
    Column('philosophy', Boolean),
    Column('books', Boolean),
    Column('films', Boolean),
    Column('songs', Boolean),
    Column('motivation', Boolean),
    Column('art', Boolean),
    Column('poems', Boolean),
    Column('love', Boolean),
    Column('humor', Boolean),
    Column('user_id', Integer),
)

user = Table('user', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('username', String(length=64)),
    Column('password', String(length=64)),
    Column('business', Boolean),
    Column('sport', Boolean),
    Column('guys', Boolean),
    Column('girls', Boolean),
    Column('philosophy', Boolean),
    Column('books', Boolean),
    Column('films', Boolean),
    Column('songs', Boolean),
    Column('motivation', Boolean),
    Column('art', Boolean),
    Column('poems', Boolean),
    Column('humor', Boolean),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['quote'].columns['humor'].create()
    post_meta.tables['user'].columns['humor'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['quote'].columns['humor'].drop()
    post_meta.tables['user'].columns['humor'].drop()
